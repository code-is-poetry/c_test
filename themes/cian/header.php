<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cian
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'cian'); ?></a>

    <header id="masthead" class="site-header bg-gray">
        <div class="container-fluid container-md">

            <div class="row align-items-center">
                <div class="col py-3 py-lg-5">
                    <?php
                    $custom_logo_id = get_theme_mod('custom_logo');
                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    ?>
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                    <?php
                    if (has_custom_logo()) {
                        echo '<img src="' . esc_url($logo[0]) . '" alt="Логотип ' . get_bloginfo('name') . '" width="160">';
                    } else {
                        echo '<h1>' . get_bloginfo('name') . '</h1>';
                    }
                    ?>
                    </a>
                </div>
                <div class="col py-3 py-lg-5 text-right">
                    <a class="header-telega" href="https://t.me/cian_realtor" target="_blank">Telegram-канал</a>
                </div>
            </div>

        </div><!-- .site-branding -->
    </header><!-- #masthead -->
