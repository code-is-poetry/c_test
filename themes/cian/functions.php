<?php
/**
 * cian functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cian
 */

if (!defined('_S_VERSION')) {
    // Версия релиза лендинга.
    define('_S_VERSION', '1.0.0');
}

if (!defined('CIAN_THEME')) {
    // Путь к директории с темой
    define('CIAN_THEME', get_template_directory_uri());
}

if (!function_exists('cian_setup')) :
    /**
     * Преднастройки функционала сайта
     */
    function cian_setup()
    {

        /*
         * Поддержка мета-тега <title>.
         */
        add_theme_support('title-tag');

        /*
         * Включаем поддержку миниатюр.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // Регистрация меню.
        register_nav_menus(
            array(
                'menu-1' => esc_html__('Primary', 'cian'),
            )
        );

        /**
         * Поддержка и преднастройки лого.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support(
            'custom-logo',
            array(
                'height' => 160,
                'width' => 59,
                'flex-width' => true,
                'flex-height' => true,
            )
        );
    }
endif;
add_action('after_setup_theme', 'cian_setup');


/**
 *  Регистрация области для виджетов.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cian_widgets_init()
{
    register_sidebar(
        array(
            'name' => esc_html__('Sidebar', 'cian'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here.', 'cian'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
}

add_action('widgets_init', 'cian_widgets_init');

/**
 * Включение на фронте скриптов и стилей.
 */
function cian_scripts()
{
    wp_enqueue_style('cian-bs-reboot', CIAN_THEME . '/css/bootstrap-reboot.min.css', array(), '4.5.2');
    wp_enqueue_style('cian-bs-grid', CIAN_THEME . '/css/bootstrap-grid.min.css', array(), '4.5.2');

    wp_enqueue_style('cian-style', get_stylesheet_uri(), array(), _S_VERSION);
    wp_style_add_data('cian-style', 'rtl', 'replace');

    //wp_enqueue_script('jquery-ui-datepicker', array('jquery'));
    wp_enqueue_script('cian-inputmask', CIAN_THEME . '/js/jquery.inputmask.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('cian-front-page', CIAN_THEME . '/js/front-page.js', array('cian-inputmask'), _S_VERSION, true);

}

add_action('wp_enqueue_scripts', 'cian_scripts');

/**
 * Разрешаем загрузку SVG
 */
function cian_allow_svg($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';

    return $mimes;
}

add_filter('upload_mimes', 'cian_allow_svg');


/**
 * Очистка области HEAD от лишнего мусора )))
 */
add_action('wp_loaded', function () {
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_resource_hints', 2);
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'wp_generator');
    add_filter('the_generator', '__return_empty_string');
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action('wp_head', 'rest_output_link_wp_head', 10, 0);
    remove_action('wp_head', 'wp_oembed_add_discovery_links');
    remove_action('template_redirect', 'rest_output_link_header', 11, 0);

});

/**
 * Удаляем небезопасный протокол XMLRPC Pingback Ping
 */
add_filter('xmlrpc_methods', 'remove_xmlrpc_pingback_ping');

function remove_xmlrpc_pingback_ping($methods)
{
    unset($methods['pingback.ping']);
    return $methods;
}

// откл
add_filter('xmlrpc_enabled', '__return_false');

/**
 * Выключаем emoji's
 */
function disable_emojis()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
    add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
}

add_action('init', 'disable_emojis');

/**
 * Удаляем emoji из  tinymce редактора.
 */
function disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    }

    return array();
}

/**
 * Удаляем emoji CDN имя хостов из DNS.
 */
function disable_emojis_remove_dns_prefetch($urls, $relation_type)
{
    if ('dns-prefetch' == $relation_type) {
        // Strip out any URLs referencing the WordPress.org emoji location
        $emoji_svg_url_bit = 'https://s.w.org/images/core/emoji/';
        foreach ($urls as $key => $url) {
            if (strpos($url, $emoji_svg_url_bit) !== false) {
                unset($urls[$key]);
            }
        }
    }
    return $urls;
}


/**
 * Валидация и проверка вопросов викторины
 */
add_filter('acf/validate_value/name=field-1', 'cian_acf_validate_value', 10, 4);
//add_filter('acf/validate_value/name=field-2', 'cian_acf_validate_value', 10, 5);
//add_filter('acf/validate_value/name=field-3', 'cian_acf_validate_value', 10, 6);

function cian_acf_validate_value($valid, $value, $field, $input)
{
    // init переменных для результата вопроса
    $q1 = false;
    $q2 = false;
    $q3 = false;

    // вопрос 1
    if ($input == 'acf[field_5f52ac6eaf0c2]' && $value == '1') {
        $q1 = true;
    }
    // вопрос 2
    if ($input == 'acf[field_5f52ac896a0bb]' && $value == '1') {
        $q2 = true;
    }
    // вопрос 3
    if ($input == 'acf[field_5f57fd80ebc61]' && $value == '20200901') {
        $q3 = true;
    }


    if ($q1 == true && $q2 == true && $q3 == true) {

        cian_get_coupon();

    } else {

    }

    return $valid;
}


/**
 * Функционал подтверждения через почту.
 */
const PREFIX = 'email-confirmation-';

function cian_send($to, $subject, $message, $headers)
{
    $token = sha1(uniqid());

    $oldData = get_option(PREFIX . 'data') ?: array();
    $data = array();
    $data[$token] = $_POST;
    update_option(PREFIX . 'data', array_merge($oldData, $data));

    wp_mail($to, $subject, sprintf($message, $token), $headers);
}

function cian_check($token)
{
    $data = get_option(PREFIX . 'data');
    $userData = $data[$token];

    if (isset($userData)) {
        unset($data[$token]);
        update_option(PREFIX . 'data', $data);
    }

    return $userData;
}

// Switch to HTML formatted email
add_filter('wp_mail_content_type', function () {
    return 'text/html';
});

/**
 * При сохранении форм.
 */
function cian_save_post($post_id)
{
    if (empty($_POST['acf']) || empty($_POST['acf']['field_5f52ac6eaf0c2'])) {
        //return;
    }
    // проверка на тип поста вп
    if (get_post_type($post_id) !== 'webinar') {
        return;
    }
    // если админка то выгоняем пчп
    if (is_admin()) {
        return;
    }
    // vars
    $post = get_post($post_id);

    // get custom fields (field group exists for content_form)
    $name1 = get_field('cian_date', $post_id);
    $name2 = get_field('cian_email', $post_id);
    $email = 'karskiy@gmail.com';

    // email data
    $to = 'karskiy@gmail.com';
    $headers = 'From: ' . $name1 . ' <' . $email . '>' . "\r\n";
    $subject = $post->post_title;
    $message = '<h1>Благодарим за вашу заявку. Пожалуйста <a href="http://shavrin.beget.tech/?token=%s">подтвердите</a> ваше намерение участвовать в вебинаре.</h1>';
    $body = $message . "\r\n" . $name1 . "\r\n" . $email . "\r\n" . $name2;

    // send email
    //wp_mail($to, $subject, $body, $headers);
    cian_send($to, $subject, $body, $headers);

}

add_action('acf/save_post', 'cian_save_post');


if (isset($_GET["token"])) {
    $data = cian_check($_GET['token']); // $_POST saved for this token
    if ($data) {
        ?>
        <script>
            alert('Это работает!');
        </script>
        <?php
    }
}


/**
 * Регистрируем тип записи Вебинар.
 */
function cian_custom_postype()
{
    $cian_args = array(
        'labels' => array('name' => esc_attr__('Регистрации')),
        'menu_icon' => 'dashicons-email',
        'show_in_admin_bar' => false,
        'public' => false,
        'rewrite' => false,
        'query_var' => false,
        'can_export' => true,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'show_in_rest' => true,
        'capability_type' => 'post',
        'capabilities' => array('create_posts' => 'do_not_allow'),
        'map_meta_cap' => true,
        'supports' => array('title', 'editor')
    );
    register_post_type('webinar', $cian_args);
}

add_action('init', 'cian_custom_postype');


/**
 * Произвольные калонки в админке для регистрации.
 */
function cian_custom_columns($columns)
{
    $columns['tel_column'] = esc_attr__('Телефон');
    $columns['email_column'] = esc_attr__('Email');
    $columns['date_column'] = esc_attr__('Дата');
    $columns['reg_column'] = esc_attr__('Подтв.');
    $custom_order = array('cb', 'title', 'tel_column', 'email_column', 'date_column', 'reg_column');
    foreach ($custom_order as $colname) {
        $new[$colname] = $columns[$colname];
    }
    return $new;
}

add_filter('manage_webinar_posts_columns', 'cian_custom_columns', 10);


/**
 * Присваеваем значения.
 */
function cian_custom_columns_content($column_name, $post_id)
{
    if ('tel_column' == $column_name) {
        $tel = get_field('cian_tel', $post_id);
        echo $tel;
    }
    if ('email_column' == $column_name) {
        $email = get_field('cian_email', $post_id);
        echo $email;
    }
    if ('date_column' == $column_name) {
        $date = get_field('cian_date', $post_id);
        echo $date;
    }

    if ('reg_column' == $column_name) {
        $reg = get_post_meta($post_id, 'cian_reg', true);
        echo $reg;
    }
}

add_action('manage_webinar_posts_custom_column', 'cian_custom_columns_content', 10, 2);

/**
 * Меняем очередность колонок.
 */
function cian_column_register_sortable($columns)
{
    $columns['email_column'] = 'cian_email';
    $columns['tel_column'] = 'cian_tel';
    $columns['date_column'] = 'cian_date';

    return $columns;
}

add_filter('manage_edit-webinar_sortable_columns', 'cian_column_register_sortable');

/**
 * Сортировка значений в колонке.
 */
function cian_tel_column_orderby($vars)
{
    if (is_admin()) {
        if (isset($vars['orderby']) && 'cian_tel' == $vars['orderby']) {
            $vars = array_merge($vars, array(
                'meta_key' => 'cian_tel',
                'orderby' => 'meta_value'
            ));
        }
    }

    return $vars;
}

add_filter('request', 'cian_tel_column_orderby');

/**
 * Сортировка значений в колонке.
 */
function cian_email_column_orderby($vars)
{
    if (is_admin()) {
        if (isset($vars['orderby']) && 'cian_email' == $vars['orderby']) {
            $vars = array_merge($vars, array(
                'meta_key' => 'cian_email',
                'orderby' => 'meta_value'
            ));
        }
    }

    return $vars;
}

add_filter('request', 'cian_email_column_orderby');

/**
 * Сортировка значений в колонке.
 */
function cian_date_column_orderby($vars)
{
    if (is_admin()) {
        if (isset($vars['orderby']) && 'cian_date' == $vars['orderby']) {
            $vars = array_merge($vars, array(
                'meta_key' => 'cian_date',
                'orderby' => 'meta_value'
            ));
        }
    }

    return $vars;
}

add_filter('request', 'cian_date_column_orderby');


/**
 * Получаем АйПи юзера.
 */
function cian_get_the_ip()
{
    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}


/**
 * Регистрация фронтэнд-форм ACF.
 */
function cian_acf_form_init()
{
    // Check function exists.
    if (function_exists('acf_register_form')) {

        // Register form.
        acf_register_form(array(
            'id' => 'cian-quiz',
            //'post_id' => 44,
            'field_groups' => ['group_5f52ac5fe4b6b'],
            'updated_message' => 'Профессионала сразу видно! Поздравляем, вы ответили на все вопрсы! Ваш промокод:  NNNNN',
            'submit_value' => 'Ответить'
        ));

        acf_register_form(array(
            'id' => 'cian-feedback',
            'post_id' => 'new_post',
            'post_title' => false,
            'post_content' => false,
            'field_groups' => ['group_5f52c4f4e440e'],
            'new_post' => array(
                'post_type' => 'webinar',
                'post_status' => 'draft',
                'post_title' => cian_get_the_ip(),
                'meta_input' => array(
                    'cian_reg' => 0,
                ),
            ),
            'updated_message' => 'Благодарим за регистрацию. На указанный вами адрес электронной почты отправлено подтверждение.',
            //'return'		=> home_url('contact-form-thank-you'),
            'submit_value' => 'Зарегистрироваться'
        ));
    }
}

add_action('acf/init', 'cian_acf_form_init');


// https://support.advancedcustomfields.com/forums/topic/validation-a-front-end-form/


/**
 * Парсим файлик CSV
 * TODO сделать интерфейс.
 */
function cian_csv_to_post()
{
    $file = CIAN_THEME . '/inc/file.csv';

    $csv = array_map('str_getcsv', file($file));

    array_walk($csv, function (&$a) use ($csv) {
        $a = array_combine($csv[0], $a);
    });

    array_shift($csv); # remove column header
    //print_r($csv );

    foreach ($csv as $row) {
        foreach ($row as $key => $line) {
            // Создаем массив данных новой записи
            $post_data = array(
                'post_title' => wp_strip_all_tags($line),
                'post_content' => $line,
                'post_status' => 'publish',
                'post_author' => 1,
                'meta_input' => ['on_off' => '0'],
            );

            // Вставляем запись в базу данных
            $post_id = wp_insert_post($post_data);
        }
    }
}

//add_action('init', 'cian_csv_to_post');


/**
 * Считаем неиспользованные купоны в DB.
 */
function cian_count_coupon()
{
    global $wpdb;
    $couponCount = $wpdb->get_col("SELECT COUNT( p.ID ) FROM {$wpdb->posts} as p INNER JOIN {$wpdb->postmeta} as m on m.post_id = p.ID WHERE p.post_type = 'post ' AND m.meta_key = 'on_off' AND m.meta_value = '0'");

    //var_dump($couponCount);

    if (empty($couponCount)) {
        return;
    } else {
        echo implode($couponCount);
    }
}


/**
 * Выборка свободного купона из БД.
 */
function cian_get_coupon()
{
    global $wpdb;

    $couponGet = $wpdb->get_results("SELECT p.ID, p.post_title FROM {$wpdb->posts} as p INNER JOIN {$wpdb->postmeta} as m on m.post_id = p.ID WHERE p.post_type = 'post ' AND m.meta_key = 'on_off' AND m.meta_value = '0' LIMIT 1", ARRAY_A);

    $cianID = ($couponGet[0]['ID']) ? $couponGet[0]['ID'] : '';
    $cianTitle = $couponGet[0]['post_title'];

    // Обновим значение мета у выбранного купона
    update_post_meta($cianID, 'on_off', '1');

    return $cianTitle;
}

/**
 * Deletes all posts from "products" custom post type.
 */
function get_delete_old_post()
{
    // WP_Query arguments
    $args = array(
        'post_type' => array('post'), //post type if you are using default than it will be post
        'posts_per_page' => '-1',//fetch all posts,

    );

    // The Query
    $query = new WP_Query($args);

    // The Loop
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            //delete post code
            //wp_trash_post( get_the_ID() );  use this function if you have custom post type
            wp_delete_post(get_the_ID(), true); //use this function if you are working with default posts
        }
    } else {
        // no posts found
        return false;

    }
    die();
    // Restore original Post Data
    wp_reset_postdata();

}

//add_action('init', 'get_delete_old_post');