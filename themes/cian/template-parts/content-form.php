<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cian
 */
global $wpdb;

$submission_ids = $wpdb->get_col( "SELECT p.ID FROM {$wpdb->posts} as p INNER JOIN {$wpdb->postmeta} as m on m.post_id = p.ID WHERE p.post_type = 'submission' AND m.meta_key = 'tel_sub'" );

var_dump($submission_ids);

// проверим метод отправки и его сущ.
if ( isset($_SERVER['REQUEST_METHOD']) && 'POST' == strtoupper($_SERVER['REQUEST_METHOD']) && isset($_POST['cian_send']) ) {

    echo 'ЭТО ПОСТ!';

    // тема письма для нас и получателя
    $post_subject = 'Регистрация на вебинар';

    // принимаем и очищаем данные
    $post_data = array(
        'form_tel' => sanitize_text_field($_POST['cian_tel']),
        'form_email' => sanitize_email($_POST['cian_email']),
        'form_date' => sanitize_textarea_field($_POST['cian_date'])
    );

    $ip_address = sprintf( esc_attr__( 'IP: %s', '' ), cian_get_the_ip() );

    // информация в БД
    $post_information = array(
        'post_title' => wp_strip_all_tags($post_subject),
        'post_content' => $post_data['form_tel'] . "\r\n\r\n" . $post_data['form_email'] . "\r\n\r\n" . $post_data['form_date'] . "\r\n\r\n" . $ip_address,
        'post_type' => 'submission',
        'post_status' => 'pending',
        'meta_input' => array( "tel_sub" => $post_data['form_tel'], "email_sub" => $post_data['form_email'], "date_sub" => $post_data['form_date'] )
    );
    wp_insert_post($post_information);

    // отправляем письма
    $to = [get_option( 'admin_email' ), $post_data['form_email']];
    $subject = "{$post_subject} - С сайта " . get_bloginfo('name');
    $body = 'Уважаемый(ая) вы зарегались на вебинар, который состоится ' . $post_data['form_date'] . ' в Городе-Герои Ленинграде';
    $headers = array('Content-Type: text/html; charset=UTF-8');

    wp_mail( $to, $subject, $body, $headers );
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


    <div id="respond" class="w-100">

        <form id="cian-test" action="<?php $_SERVER['REQUEST_URI']; ?>" method="post">

            <div class="form-group tel-name-group">
                <label for="msg_tel">Ваш номер телефона:</label>
                <input type="tel" name="cian_tel" id="cian-tel" required>

            </div>

            <div class="form-group email-name-group">
                <label for="msg_email">Ваш Email:</label>
                <input type="email" name="cian_email" id="cian-email" required>
            </div>

            <div class="form-group date-name-group">
                <label for="msg_email">Дата:</label>
                <input type="j-date" name="cian_date" id="datepicker" required>
            </div>


            <div class="form-group vscf-privacy-group">
                <input type="hidden" name="cian_privacy" id="cian-privacy-hidden" value="no">
                <label>
                    <input type="checkbox" name="cian_privacy" id="cian-privacy" class="custom-control-input" value="yes" <?php checked( esc_attr($form_data['form_privacy']), "yes", false ); ?> required> <span>Я принимаю условия</span>
                </label>
            </div>

            <div class="form-group vscf-submit-group">
                <button type="submit" name="cian_send" id="cian-send" class="btn btn-primary">Отправить</button>
            </div>

        </form>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->
