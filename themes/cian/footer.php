<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cian
 */

?>

<footer id="colophon" class="site-footer py-5 m-0">
    <div class="site-info container">
        <?php
        $cian_description = get_bloginfo('description', 'display');
        if ($cian_description || is_customize_preview()) :
            ?>
            <p class="m-0 p-0 text-white text-center">
                &copy; <?php echo bloginfo('name') . ' - ' . $cian_description . ', ' . absint(date('Y')); ?></p>
        <?php endif; ?>
    </div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
