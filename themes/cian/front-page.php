<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cian
 */
acf_form_head();
get_header();

//cian_csv_to_post();

//cian_get_coupon();
?>

    <main id="primary" class="site-main">

        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <p class="fs-52 font-weight-bold blue-light mb-2">Что? <span class="blue-dark">Где?</span> Когда?
                    </p>
                    <p class="fs-32 my-0">Викторина для профессионалов рынка недвижимости</p>
                </div>
                <div class="col">
                    <img src="<?php echo CIAN_THEME . '/img/girl.png'; ?>" alt="Картинка">
                </div>
            </div>
        </div>

        <div class="bg-blue-light mt-3 mt-lg-5 py-3 py-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md">
                        <p class="fs-24 m-0 p-0 text-white">
                            <span>Подписка — формат размещения объявлений из пакета с фиксированной стоимостью и <span
                                        class="bg-blue-dark pb-1 px-2">дополнительной скидкой</span> на размещение.</span>
                        </p>
                    </div>
                    <div class="col-12 col-md">
                        <p class="fs-24 m-0 p-0 text-white">
                            <span>Ответьте правильно на 3 вопроса о Подписках и получите промокод <span
                                        class="bg-blue-dark pb-1 px-2">на бесплатное </span>размещение объявлений на Циан!</span>
                        </p>
                    </div>

                    <div class="col-12 mt-5">
                       <p class="text-center text-white fs-32 m-0 p-0">Спешите, осталось <span class="bg-blue-dark pb-1 px-2"><?php cian_count_coupon();?></span> купонов</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="container py-3 py-lg-5">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <?php
                    acf_form('cian-quiz');
                    ?>
                </div>
            </div>
        </div>

        <div class="bg-gray mb-3 mb-lg-5 py-3 py-lg-5">
            <div class="container">
                <h2 class="font-weight-bold fs-32 blue-dark"><span class="blue-light">Хотите узнать</span> о всех
                    преимуществах Подписок?</h2>
                <p class="fs-24">Бесплатные семинары помогут лучше разобраться в работе с продуктами и сервисами Циан и
                    увеличить отдачу от работы с площадкой</p>
            </div>
        </div>

        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <p class="fs-52 font-weight-bold blue-light mb-2">Циан.<span class="blue-dark">Семинар</span></p>
                    <p class="fs-32 my-0">
                        «Как работать с подписками на Циан?»
                        <br>
                        <span class="bg-blue-dark text-white pb-1 px-2">8 сентября в 12:00 (МСК)</span>
                    </p>
                </div>
                <div class="col">
                    <img src="<?php echo CIAN_THEME . '/img/alla.png'; ?>" alt="Картинка">
                </div>
            </div>
        </div>

        <div class="bg-gray my-3 my-lg-5 py-3 py-lg-5">
            <div class="container">
                <p class="fs-32 m-0 p-0">
                    <span class="blue-light font-weight-bold">На вебинаре <span class="blue-dark">обсудим</span></span>:
                <ul class="fs-24">
                    <li>✔ какие преимущества имеет подписка</li>
                    <li>✔ как рассчитывается стоимость подписки</li>
                    <li>✔ что такое уровни подписки</li>
                    <li>✔ как получить максимальную скидку и кэшбек до 50%</li>
                </ul>
                </p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <?php
                    acf_form('cian-feedback');
                    ?>
                </div>
            </div>
        </div>

        <div class="bg-gray mt-3 mt-lg-5 py-3 py-lg-5">
            <div class="container">
                <p class="fs-32 m-0 p-0">
                    <span class="font-weight-bold">Если у вас</span> <span class="blue-light font-weight-bold">остались вопросы, <span
                                class="blue-dark">напишите нам</span></span>
                    <a href="https://www.cian.ru/contacts/" rel="nofollow" target="_blank" class="ml-3">Написать</a>
                </p>
                <p class="fs-24">
                    <a href="https://hc.cian.ru/hc/ru/" rel="nofollow" target="_blank">Дополнительная информация в
                        справочном центре</a> или в @cian_support_bot в телеграм
                </p>
            </div>
        </div>

    </main><!-- #main -->

<?php
get_footer();
